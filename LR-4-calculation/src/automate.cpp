#include "automate.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;

void generate_normal_dist(normal_distribution dist, 
					std::vector<std::vector<automate>> & state) {

	srand(time(NULL));

	const double m_1 = dist.m_1;
	const double m_2 = dist.m_1;
	const double sigma_1 = dist.sigma_1;
	const double sigma_2 = dist.sigma_2;
	auto f = [m_1, m_2, sigma_1, sigma_2](double x, double y) {
		return 1 / (2 * M_PI) * exp(-(x - m_1) * (x - m_1) / (2 * sigma_1) -
										(y - m_2) * (y - m_2) / (2 * sigma_2));
	};

	for (int i = 0; i < state.size(); ++i)
		for (int j = 0; j < state[i].size(); ++j) {
			for (int k = 0; k < 10 ; ++k) {
				float tmp = float(rand() % 100 + 1) / 100;
				double p = f(i, j);
				if (tmp <= p)
					state[i][j].alive = 1;
			}
		}
}

void generate_even_dist(even_distribution dist,
					std::vector<std::vector<automate>> & state) {

	srand(time(NULL));

	size_t cells = dist.count;
	int rand_i, rand_j;
	for (int i = 0; i < cells; ++i) {
		rand_i = rand() % (state.size());
		rand_j = rand() % (state[0].size());
		state[rand_i][rand_j].alive = 1;
	}
}

std::vector<std::vector<automate>> generate(const configuration & config) {
	
	std::vector<std::vector<automate>> state(config.size.x);
	for (int i = 0; i < state.size(); ++i)
		state[i].resize(config.size.y);

	switch (config.distribution->type())
	{
	case distribution_type::normal:
		generate_normal_dist(
			*dynamic_cast<normal_distribution*>(config.distribution.get()),
			state
		);
		break;
	case distribution_type::even:
		generate_even_dist(
			*dynamic_cast<even_distribution*>(config.distribution.get()),
			state
		);
		break;
	default:
		break;
	}

	return state;
}

void automate::release_tact() {
	switch (mode) {
	case automate_mode::neural_network:
		if (isActive) {
			active_ta�ts++;
			if (active_ta�ts == 5) {
				isActive = 0;
				alive = 0;
				active_ta�ts = 0;
				isRestore = 1;
			}
		}
		else if (isRestore) {
			active_power *= 0.3;
			restore_ta�ts++;
			if (restore_ta�ts == 8) {
				restore_ta�ts = 0;
				isRestore = 0;
			}
		}
		break;

	case automate_mode::organoid:
		
		if (alive) {
			if (cell_enerjy > automate::absorption && 
					organoid_enerjy + automate::absorption< p_max) {
				organoid_enerjy += automate::absorption;
				cell_enerjy -= automate::absorption;
			}
			organoid_enerjy -= automate::tackt_enj;

			if (organoid_enerjy > 0) {
				life_time++;
				if (life_time > automate::max_lifetime)
					alive = false;
				else if (life_time > automate::adult_age)
					isAdult = true;
			}
			else {
				alive = false;
				life_time = 0;
				organoid_enerjy = 0;
			}
			moved = false;
			duplicated = false;
		}
		else {
			cell_enerjy += automate::r;
		}
		break;

	default:
		break;
	}
		
}

void automate::activate() {
	if (!isRestore && !isActive) {
		active_ta�ts = 0;
		restore_ta�ts = 0;
		isRestore = 0;
		isActive = 1;
		active_power = 1;
		alive = 1;
	}
}