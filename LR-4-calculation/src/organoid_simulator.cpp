#include "automate.hpp"
#include "organoid_simulator.hpp"

#include <iostream>
#include <time.h>
#include <iterator>
#include <vector>
#include <algorithm>

using namespace std;

struct point2 {
	uint32_t i, j;
	point2(int i, int j) {
		this->i = i;
		this->j = j;
	}
};

vector<point2> getEmptyCells(vector<vector<automate>> & state, uint32_t i, uint32_t j) {
	vector<int32_t> neighbors_x, neighbors_y;
	vector<point2> emptyCells;

	if (i == 0) {
		neighbors_x = { 0, 1 };
	}
	else if (i == state.size() - 1) {
		neighbors_x = { 0, -1 };
	}
	else {
		neighbors_x = { 1, 0, -1 };
	}

	if (j == 0) {
		neighbors_y = { 0, 1 };
	}
	else if (j == state[0].size() - 1) {
		neighbors_y = { 0, -1 };
	}
	else {
		neighbors_y = { 1, 0, -1 };
	}

	for each (uint32_t p in neighbors_x)
		for each (uint32_t q in neighbors_y) {
			if (p == 0)
				if (q == 0)
					continue;
			if (!state[i + p][j + q].alive)
				emptyCells.push_back(point2(i + p, j + q));
		}
	
	return emptyCells;
}

point2 randCell(vector<point2> emptyPoints) {
	point2 res(0, 0);

	size_t count = emptyPoints.size();
	size_t rand_elem = rand() % count;

	return emptyPoints[rand_elem];
}

point2 getTheReachestCell(const vector<vector<automate>> & state,
		vector<point2> emptyCells) {

	uint32_t max = 0;
	point2 res(0, 0);

	for (auto cell : emptyCells)
		if (state[cell.i][cell.j].cell_enerjy > max) {
			max = state[cell.i][cell.j].cell_enerjy;
			res = cell;
		}

	return res;
}

organoid_simulator::organoid_simulator(const configuration & config) {
	this->config = config;
	current_state = generate(config);
	for (uint32_t i = 0; i < current_state.size(); ++i) {
		for (uint32_t j = 0; j < current_state[i].size(); ++j) {
			current_state[i][j].mode = automate_mode::organoid;
		}
	}
	copy(current_state.begin(), current_state.end(), back_inserter(prev_state));
}

void swap_ordanoid(automate & first, automate & second) {
	swap(first.alive, second.alive);
	swap(first.life_time, second.life_time);
	swap(first.organoid_enerjy, second.organoid_enerjy);
	swap(first.moved, second.moved);
}

vector<vector<automate>> & organoid_simulator::next_state() {
	if (config.isOrganoidsSmart)
		return get_state_smart();
	else 
		return get_state_stupid();
}

vector<vector<automate>> & organoid_simulator::get_state() {
	return current_state;
}

vector<vector<automate>> & organoid_simulator::get_state_stupid() {

	uint32_t row = current_state.size();
	uint32_t col = current_state[0].size();
	uint32_t tmp = 0;

	for (uint32_t i = 0; i < row; ++i)
		for (uint32_t j = 0; j < col; ++j) {
			if (current_state[i][j].alive) {
				auto empty_cells = getEmptyCells(current_state, i, j);
				if (empty_cells.size() > 0) {
					auto new_cell = randCell(empty_cells);
					if (current_state[i][j].isAdult && !current_state[i][j].duplicated) {
						swap_ordanoid(current_state[new_cell.i][new_cell.j], current_state[i][j]);
						current_state[i][j].alive = 1;
						current_state[new_cell.i][new_cell.j].organoid_enerjy -= automate::duplication_erj;
						current_state[new_cell.i][new_cell.j].moved = true;
						current_state[new_cell.i][new_cell.j].duplicated = true;
					}
					else if (!current_state[i][j].moved) {
						swap_ordanoid(current_state[new_cell.i][new_cell.j], current_state[i][j]);
						current_state[new_cell.i][new_cell.j].moved = 1;
					}
				}
			}
		}

	for (uint32_t i = 0; i < row; ++i)
		for (uint32_t j = 0; j < col; ++j)
			current_state[i][j].release_tact();

	return current_state;
}

vector<vector<automate>> & organoid_simulator::get_state_smart() {

	uint32_t row = current_state.size();
	uint32_t col = current_state[0].size();
	uint32_t tmp = 0;

	for (uint32_t i = 0; i < row; ++i)
		for (uint32_t j = 0; j < col; ++j) {
			if (current_state[i][j].alive) {
				auto empty_cells = getEmptyCells(current_state, i, j);
				if (empty_cells.size() > 0) {
					auto new_cell = getTheReachestCell(current_state, empty_cells);
					if (current_state[i][j].isAdult && !current_state[i][j].duplicated) {
						swap_ordanoid(current_state[new_cell.i][new_cell.j], current_state[i][j]);
						current_state[i][j].alive = 1;
						current_state[new_cell.i][new_cell.j].organoid_enerjy -= automate::duplication_erj;
						current_state[new_cell.i][new_cell.j].moved = true;
						current_state[new_cell.i][new_cell.j].duplicated = true;
					}
					else if (!current_state[i][j].moved) {
						swap_ordanoid(current_state[new_cell.i][new_cell.j], current_state[i][j]);
						current_state[new_cell.i][new_cell.j].moved = 1;
					}
				}
			}
		}

	for (uint32_t i = 0; i < row; ++i)
		for (uint32_t j = 0; j < col; ++j)
			current_state[i][j].release_tact();

	return current_state;
}