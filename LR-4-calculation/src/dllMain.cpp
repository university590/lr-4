#include "dotNetMatrix.hpp"
#include "life_simulator.hpp"
#include "neural_network.hpp"
#include "organoid_simulator.hpp"
#include <iostream>
#include <memory>

using namespace std;

extern "C" _declspec(dllexport) int 
dotNetMatrix_get(dotNetMatrix *obj, int i, int j) {
	return (int)obj->get(i, j);
}

extern "C" _declspec(dllexport) normal_distribution*
create_normal_distribution(double m_1, double m_2, double sigma_1, double sigma_2) {
	return new normal_distribution(m_1, m_2, sigma_1, sigma_2);
}

extern "C" _declspec(dllexport) even_distribution*
create_even_distribution(int count) {
	return new even_distribution(count);
}

extern "C" _declspec(dllexport) configuration*
create_configuration_normal_dist(bool ompSupport, int x, int y, 
					const normal_distribution *dist) {
	dim_2 size;
	size.x = x;
	size.y = y;
	return new configuration(ompSupport, size, *dist);
}

extern "C" _declspec(dllexport) configuration*
create_configuration_even_dist(bool ompSupport, int x, int y,
					const even_distribution *dist) {
	dim_2 size;
	size.x = x;
	size.y = y;
	return new configuration(ompSupport, size, *dist);
}

extern "C" _declspec(dllexport) configuration*
create_configuration(bool ompSupport, int x, int y) {
	dim_2 size;
	size.x = x;
	size.y = y;
	return new configuration(ompSupport, size, even_distribution(0));
}

extern "C" _declspec(dllexport) void
add_front(configuration *config, double k, double b) {
	config->addFront(front(k, b));
}

extern "C" _declspec(dllexport) void
isSmartOrganoidsEnabled(configuration *config, bool enabled) {
	config->isSmartOrganoidsEnabled(enabled);
}

extern "C" _declspec(dllexport) life_simulator* 
create_life_simulator(const configuration *config) {
	return new life_simulator(*config);
}

extern "C" _declspec(dllexport) dotNetMatrix*
get_state(life_simulator *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->get_state()));
}

extern "C" _declspec(dllexport) dotNetMatrix*
get_next_state(life_simulator *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->next_state()));
}

extern "C" _declspec(dllexport) void
setLifeAutomate(life_simulator *obj, uint32_t i, uint32_t j) {
	return obj->setAutomate(i, j);
}

extern "C" _declspec(dllexport) neural_activator*
create_neural_activator(int x, int y) {
	return new neural_activator(x, y);
}

extern "C" _declspec(dllexport) neural_network*
create_neural_network(const configuration *config, const neural_activator *atc) {
	return new neural_network(*config, *atc);
}

extern "C" _declspec(dllexport) dotNetMatrix*
n_get_state(neural_network *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->get_state()));
}

extern "C" _declspec(dllexport) dotNetMatrix*
n_get_next_state(neural_network *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->next_state()));
}

extern "C" _declspec(dllexport) organoid_simulator*
organoid_simulator_network(const configuration *config) {
	return new organoid_simulator(*config);
}

extern "C" _declspec(dllexport) dotNetMatrix*
o_get_state(organoid_simulator *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->get_state()));
}

extern "C" _declspec(dllexport) dotNetMatrix*
o_next_state(organoid_simulator *obj) {
	return new dotNetMatrix(shared_ptr<vector<vector<automate>>>(&obj->next_state()));
}

extern "C" _declspec(dllexport) void
delete_matrix(dotNetMatrix *obj) {
	delete obj;
}