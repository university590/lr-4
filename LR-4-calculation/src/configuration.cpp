#include "configuration.hpp"

using namespace std;

configuration::configuration(bool ompSupport, dim_2 size,
	normal_distribution distribution) {

	enableOMP = ompSupport;
	this->size = size;
	this->distribution.reset(new normal_distribution(distribution.m_1,
		distribution.m_2, distribution.sigma_1, distribution.sigma_2));
}
configuration::configuration(bool ompSupport, dim_2 size,
	even_distribution distribution) {

	enableOMP = ompSupport;
	this->size = size;
	this->distribution.reset(new even_distribution(distribution.count));
}


void configuration::addFront(const front & front) {
	fronts.push_back(front);
}

void configuration::isSmartOrganoidsEnabled(bool enabled) {
	isOrganoidsSmart = enabled;
}