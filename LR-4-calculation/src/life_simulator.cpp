#include "automate.hpp"
#include "life_simulator.hpp"

#include <iterator>

using namespace std;

life_simulator::life_simulator(const configuration & config) {
	this->config = config;
	this->current_state = generate(config);
	copy(current_state.begin(), current_state.end(), back_inserter(prev_state));
}

std::vector<std::vector<automate>> & life_simulator::get_state() {
	return current_state;
}

std::vector<std::vector<automate>> & life_simulator::next_state() {

	prev_state = current_state;
	uint32_t row = current_state.size();
	uint32_t col = current_state[0].size();
	uint32_t tmp = 0;

	tmp = prev_state[0][1].alive + prev_state[1][1].alive + prev_state[1][0].alive;
	if (tmp == 2)
		current_state[0][0].alive = 1;
	else if (tmp > 2)
		current_state[0][0].alive = 0;

	tmp = prev_state[row - 1][1].alive + 
						prev_state[row - 2][1].alive + prev_state[row - 2][0].alive;
	if (tmp == 2)
		current_state[row - 1][0].alive = 1;
	else if (tmp > 2)
		current_state[row - 1][0].alive = 0;

	tmp = prev_state[row - 1][col - 2].alive + 
			prev_state[row - 2][col - 2].alive + prev_state[row - 2][col - 1].alive;
	if (tmp == 2)
		current_state[row - 1][col - 1].alive = 1;
	else if (tmp > 2)
		current_state[row - 1][col - 1].alive = 0;

	tmp = prev_state[0][col - 2].alive +
		prev_state[1][col - 2].alive + prev_state[1][col - 1].alive;
	if (tmp == 2)
		current_state[0][col - 1].alive = 1;
	else if (tmp > 2)
		current_state[0][col - 1].alive = 0;

	for (int i = 1; i < row - 1; ++i) {
		tmp = prev_state[i - 1][0].alive +
			prev_state[i - 1][1].alive + prev_state[i][1].alive + 
			prev_state[i + 1][1].alive + prev_state[i + 1][0].alive;
		if (tmp == 2)
			current_state[i][0].alive = 1;
		else if (tmp > 2)
			current_state[i][0].alive = 0;
	}

	for (int i = 1; i < row - 1; ++i) {
		tmp = prev_state[i - 1][col - 1].alive +
			prev_state[i - 1][col - 2].alive + prev_state[i][col - 2].alive +
			prev_state[i + 1][col - 2].alive + prev_state[i + 1][col - 1].alive;
		if (tmp == 2)
			current_state[i][col - 1].alive = 1;
		else if (tmp > 2)
			current_state[i][col - 1].alive = 0;
	}

	for (int j = 1; j < col - 1; ++j) {
		tmp = prev_state[0][j - 1].alive +
			prev_state[1][j - 1].alive + prev_state[1][j].alive +
			prev_state[1][j + 1].alive + prev_state[0][j + 1].alive;
		if (tmp == 2)
			current_state[0][j].alive = 1;
		else if (tmp > 2)
			current_state[0][j].alive = 0;
	}

	for (int j = 1; j < col - 1; ++j) {
		tmp = prev_state[row - 1][j - 1].alive +
			prev_state[row - 2][j - 1].alive + prev_state[row - 2][j].alive +
			prev_state[row - 2][j + 1].alive + prev_state[row - 1][j + 1].alive;
		if (tmp == 2)
			current_state[row - 1][j].alive = 1;
		else if (tmp > 2)
			current_state[row - 1][j].alive = 0;
	}

	for (int i = 1; i < row - 1; ++i)
		for (int j = 1; j < col - 1; ++j) {
			tmp = prev_state[i - 1][j - 1].alive + prev_state[i - 1][j].alive +
				prev_state[i - 1][j + 1].alive + prev_state[i][j - 1].alive +
				prev_state[i][j + 1].alive + prev_state[i + 1][j - 1].alive +
				prev_state[i + 1][j].alive + prev_state[i + 1][j + 1].alive;
			if (tmp == 2)
				current_state[i][j].alive = 1;
			else if (tmp > 2)
				current_state[i][j].alive = 0;
		}

	return current_state;
}

void life_simulator::setAutomate(uint32_t i, uint32_t j) {
	current_state[i][j].alive = true;
}