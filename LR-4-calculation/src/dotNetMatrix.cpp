#include "dotNetMatrix.hpp"
#include <iostream>

using namespace std;

dotNetMatrix::dotNetMatrix(shared_ptr<vector<vector<automate>>> & state) {
	matrix_ptr = state;
}

int dotNetMatrix::get(size_t i, size_t j) {
	return (*matrix_ptr.get())[i][j].alive;
}