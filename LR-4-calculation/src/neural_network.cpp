#include "automate.hpp"
#include "neural_network.hpp"

#include <iostream>

#include <iterator>

using namespace std;

int roundl(double _X) {
	return static_cast<int>(round(_X));
}

void activate_fronts(const configuration & config, vector<vector<automate>> & state) {
	for each (auto front in config.fronts) {

			auto y = [front](double x) { return roundl(front.k * x + front.b); };
			int end;

			if (front.k >= 0) {
				end = roundl((config.size.y - front.b) / front.k);
				if (end >= config.size.x)
					end = config.size.x;
			}
			else {
				if (-front.b / front.k < config.size.x)
					end = roundl(-front.b / front.k);
				else
					end = config.size.x;
			}

			int start;
			if (front.b >= 0)
				if (front.k >= 0)
					start = 0;
				else {
					if (front.b > config.size.y)
						start = roundl((config.size.y - front.b) / front.k);
					else
						start = 0;
				}
			else
				start = roundl(-front.b / front.k);

			for (int i = start + 1; i < end - 1; ++i)
				state[i][y(i)].activate();
	}
}

neural_network::neural_network(const configuration & config, const neural_activator & activator) {
	this->config = config;
	this->activator = activator;
	current_state = generate(config);

	for (uint32_t i = 0; i < current_state.size(); ++i)
		for (uint32_t j = 0; j < current_state[i].size(); ++j)
			current_state[i][j].mode = automate_mode::neural_network;

	activate_fronts(this->config, current_state);
	copy(current_state.begin(), current_state.end(), back_inserter(prev_state));
}

vector<vector<automate>> & neural_network::next_state() {
	prev_state = current_state;

	if (tackts % 15 == 0)
		activate_fronts(this->config, current_state);

	uint32_t row = current_state.size();
	uint32_t col = current_state[0].size();
	uint32_t tmp = 0;

	for (uint32_t i = 0; i < row; ++i)
		for (uint32_t j = 0; j < col; ++j)
			current_state[i][j].release_tact();
	activator.release_tact();
	
	if (activator.isActive)
		for (int i = -1; i <= 1; ++i)
			for (int j = -1; j <= 1; ++j)
				current_state[activator.pos_x + i][activator.pos_y + j].activate();
	
	tmp = prev_state[0][1].active_power + prev_state[1][1].active_power + prev_state[1][0].active_power;
	if (tmp >= 3)
		current_state[0][0].activate();

	tmp = prev_state[row - 1][1].active_power +
		prev_state[row - 2][1].active_power + prev_state[row - 2][0].active_power;
	if (tmp >= 3)
		current_state[row - 1][0].activate();

	tmp = prev_state[row - 1][col - 2].active_power +
		prev_state[row - 2][col - 2].active_power + prev_state[row - 2][col - 1].active_power;
	if (tmp >= 3)
		current_state[row - 1][col - 1].activate();

	tmp = prev_state[0][col - 2].active_power +
		prev_state[1][col - 2].active_power + prev_state[1][col - 1].active_power;
	if (tmp >= 3)
		current_state[0][col - 1].activate();
	
	for (uint32_t i = 1; i < row - 1; ++i) {
		tmp = prev_state[i - 1][0].active_power +
			prev_state[i - 1][1].active_power + prev_state[i][1].active_power +
			prev_state[i + 1][1].active_power + prev_state[i + 1][0].active_power;
		if (tmp >= 3)
			current_state[i][0].activate();
	}
	
	for (uint32_t i = 1; i < row - 1; ++i) {
		tmp = prev_state[i - 1][col - 1].active_power +
			prev_state[i - 1][col - 2].active_power + prev_state[i][col - 2].active_power +
			prev_state[i + 1][col - 2].active_power + prev_state[i + 1][col - 1].active_power;
		if (tmp >= 3)
			current_state[i][col - 1].activate();
	}
	
	for (uint32_t j = 1; j < col - 1; ++j) {
		tmp = prev_state[0][j - 1].active_power +
			prev_state[1][j - 1].active_power + prev_state[1][j].active_power +
			prev_state[1][j + 1].active_power + prev_state[0][j + 1].active_power;
		if (tmp >= 3)
			current_state[0][j].activate();
	}
	
	for (uint32_t j = 1; j < col - 1; ++j) {
		tmp = prev_state[row - 1][j - 1].active_power +
			prev_state[row - 2][j - 1].active_power + prev_state[row - 2][j].active_power +
			prev_state[row - 2][j + 1].active_power + prev_state[row - 1][j + 1].active_power;
		if (tmp >= 3)
			current_state[row - 1][j].activate();
	}
	
	for (uint32_t i = 1; i < row - 1; ++i)
		for (uint32_t j = 1; j < col - 1; ++j) {
			tmp = prev_state[i - 1][j - 1].active_power + prev_state[i - 1][j].active_power +
				prev_state[i - 1][j + 1].active_power + prev_state[i][j - 1].active_power +
				prev_state[i][j + 1].active_power + prev_state[i + 1][j - 1].active_power +
				prev_state[i + 1][j].active_power + prev_state[i + 1][j + 1].active_power;
			if (tmp >= 3)
				current_state[i][j].activate();
		}

	tackts++;

	return current_state;
}

vector<vector<automate>> & neural_network::get_state() {
	return current_state;
}