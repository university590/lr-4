#ifndef NEURALNETWORK
#define NEURALNETWORK

#include "automate.hpp"
#include "configuration.hpp"

#include <vector>

class neural_activator {
public:
	int pos_x, pos_y;
	int isActive = 0;
	int restore_tackts = 14;

	neural_activator(int pos_x, int pos_y) {
		this->pos_x = pos_x;
		this->pos_y = pos_y;
	}
	neural_activator() {}

	void release_tact() {
		if (isActive)
			isActive = 0;
		else {
			restore_tackts++;
			if (restore_tackts == 15) {
				isActive = 1;
				restore_tackts = 0;
			}
		}
	}

};

class neural_network {
	std::vector<std::vector<automate>> prev_state;
	std::vector<std::vector<automate>> current_state;
	neural_activator activator;
	uint32_t tackts = 1;

	configuration config;

public:
	neural_network(const configuration & config, const neural_activator & activator);
	std::vector<std::vector<automate>> & next_state();
	std::vector<std::vector<automate>> & get_state();
};

#endif