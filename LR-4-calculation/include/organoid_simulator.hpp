#ifndef ORGANOIDSIMULATOR
#define ORGANOIDSIMULATOR

#include "automate.hpp"
#include "configuration.hpp"

#include <vector>

class organoid_simulator {
	std::vector<std::vector<automate>> prev_state;
	std::vector<std::vector<automate>> current_state;

	configuration config;

public:
	organoid_simulator(const configuration & config);
	std::vector<std::vector<automate>> & next_state();
	std::vector<std::vector<automate>> & get_state();

private:
	std::vector<std::vector<automate>> & get_state_stupid();
	std::vector<std::vector<automate>> & get_state_smart();
};

#endif