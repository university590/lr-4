#ifndef DISTRIBUTION
#define DISTRIBUTION

enum class distribution_type { normal, even };

class distribution {
public:
	virtual distribution_type type() = 0;
};

class normal_distribution : public distribution {
public:
	double m_1, m_2;
	double sigma_1, sigma_2;

	normal_distribution(double m_1, double m_2, double sigma_1, double sigma_2) {
		this->m_1 = m_1;
		this->m_2 = m_2;
		this->sigma_1 = sigma_1;
		this->sigma_2 = sigma_2;
	}

	virtual distribution_type type() {
		return distribution_type::normal;
	} 
};

class even_distribution : public distribution {
public:
	int count;
	virtual distribution_type type() {
		return distribution_type::even;
	}

	even_distribution(int count) {
		this->count = count;
	}
};

#endif