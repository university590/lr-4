#ifndef CONFIGURATION
#define CONFIGURATION

#include "distribution.hpp"

#include <memory>
#include <list>

struct dim_2 {
	int x, y;
};


struct front {
	double k, b;

	front() {}
	front(double k, double b) {
		this->k = k;
		this->b = b;
	}
};

class configuration {
public:
	bool enableOMP = true;
	dim_2 size;
	std::shared_ptr<distribution> distribution;

	std::list<front> fronts;
	bool isOrganoidsSmart = false;

	configuration(bool ompSupport, dim_2 size, normal_distribution distribution);
	configuration(bool ompSupport, dim_2 size, even_distribution distribution);
	configuration() {};
	void addFront(const front & front);
	void isSmartOrganoidsEnabled(bool enabled);
};

#endif
