#ifndef AUTOMATE
#define AUTOMATE

#include "configuration.hpp"

#include <vector>

class configuration;

enum class automate_mode {lifegame, neural_network, organoid};

class automate {
public:
	automate_mode mode;

	int alive = 0;

	int isActive = 0;
	int isRestore = 0;
	int active_power = 0;
	int active_ta�ts = 0;
	int restore_ta�ts = 0;
	bool isAdult = false;

	int life_time = 0;
	int organoid_enerjy = 0;
	int cell_enerjy = 10;
	bool moved = false;
	bool duplicated = false;

	void release_tact();
	void activate();

	static const int max_lifetime = 15;
	static const int r = 1;
	static const int Pmax = 10;
	static const int A = 0.3;
	static const int p_max = 35;
	static const int absorption = 5;
	static const int tackt_enj = 2;
	static const int duplication_erj = 3;
	static const int adult_age = 3;
};

std::vector<std::vector<automate>> generate(const configuration & config);

#endif
