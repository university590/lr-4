#ifndef LIFESIMULATOR
#define LIFESIMULATOR

#include "automate.hpp"
#include "configuration.hpp"

#include <vector>

class life_simulator {
	std::vector<std::vector<automate>> prev_state;
	std::vector<std::vector<automate>> current_state;

	configuration config;

public:
	life_simulator(const configuration & config);
	std::vector<std::vector<automate>> & next_state();
	std::vector<std::vector<automate>> & get_state();
	void setAutomate(uint32_t i, uint32_t j);
};

#endif