#ifndef DOTNETMATRIX
#define DOTNETMATRIX

#include "automate.hpp"

#include <vector>
#include <memory>

class dotNetMatrix {
private:
	std::shared_ptr<std::vector<std::vector<automate>>> matrix_ptr;


public:
	dotNetMatrix(std::shared_ptr<std::vector<std::vector<automate>>> & state);
	int get(size_t i, size_t j);
};

#endif