﻿using LR_4_graphics.CalculationAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class NeuralWindow : Form
    {
        private DotNetMatrix currentState;
        private Size size;
        private NeuralNetwork simulator;
        private bool isModelling = false;

        public NeuralWindow()
        {
            InitializeComponent();
            timer1.Interval = 100;
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            try
            {
                size.Width = Convert.ToInt32(sizeXBox.Text);
                size.Height = Convert.ToInt32(sizeYBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверно задан размер.");
                return;
            }

            int activatorX, activatorY;
            try
            {
                activatorX = Convert.ToInt32(activatorPosXBox.Text);
                activatorY = Convert.ToInt32(activatorPosYBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверно заданы координаты");
                return;
            }

            var config = new Configuration(true, size, new EvenDistribution(0));

            try
            {
                var fronts = frontControl1.getFrontsList();
                config.fronts = fronts;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверно заданы линии фронта.");
                return;
            }

            simulator = new NeuralNetwork(config, activatorX, activatorY);

            currentState = simulator.getState();
            Draw();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            if (!isModelling)
            {
                isModelling = true;
                timer1.Start();
                playButton.Text = "Stop";
            }
            else
            {
                isModelling = false;
                timer1.Stop();
                playButton.Text = "Play";
            }
        }


        private void Draw()
        {
            double k;
            k = (double)pictureBox1.Height / size.Height;
            if ((int)(size.Width * k) > pictureBox1.Width)
                k = (double)pictureBox1.Width / size.Width;

            var bmp = new Bitmap((int)(k * size.Width) + 1, (int)(k * size.Height) + 1);


            var image = Graphics.FromImage(bmp);
            image.Clear(SystemColors.Control);

            var BlackPen = new Pen(Brushes.Black);

            int width = (int)k;
            int height = (int)k;

            var matrix = currentState;

            for (int i = 0; i < size.Width; ++i)
            {
                for (int j = 0; j < size.Height; ++j)
                {
                    Rectangle rect = new Rectangle(i * width, j * height, width, height);
                    image.DrawRectangle(BlackPen, rect);
                    if (currentState[i, size.Height - j - 1])
                        image.FillRectangle(Brushes.Red, rect);
                }
            }

            pictureBox1.Image = bmp;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            currentState = simulator.nextState();
            Draw();
        }

        private void NeuralWindow_SizeChanged(object sender, EventArgs e)
        {
            Draw();
        }
    }
}
