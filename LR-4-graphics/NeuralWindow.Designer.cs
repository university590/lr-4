﻿namespace LR_4_graphics
{
    partial class NeuralWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.playButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.sizeXBox = new System.Windows.Forms.TextBox();
            this.sizeYBox = new System.Windows.Forms.TextBox();
            this.activatorPosXBox = new System.Windows.Forms.TextBox();
            this.activatorPosYBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.frontControl1 = new LR_4_graphics.FrontControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(12, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1206, 484);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // playButton
            // 
            this.playButton.Location = new System.Drawing.Point(529, 21);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(75, 38);
            this.playButton.TabIndex = 5;
            this.playButton.Text = "Play";
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(448, 21);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(75, 38);
            this.generateButton.TabIndex = 4;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // sizeXBox
            // 
            this.sizeXBox.Location = new System.Drawing.Point(67, 31);
            this.sizeXBox.Name = "sizeXBox";
            this.sizeXBox.Size = new System.Drawing.Size(50, 20);
            this.sizeXBox.TabIndex = 7;
            // 
            // sizeYBox
            // 
            this.sizeYBox.Location = new System.Drawing.Point(141, 31);
            this.sizeYBox.Name = "sizeYBox";
            this.sizeYBox.Size = new System.Drawing.Size(50, 20);
            this.sizeYBox.TabIndex = 8;
            // 
            // activatorPosXBox
            // 
            this.activatorPosXBox.Location = new System.Drawing.Point(318, 31);
            this.activatorPosXBox.Name = "activatorPosXBox";
            this.activatorPosXBox.Size = new System.Drawing.Size(50, 20);
            this.activatorPosXBox.TabIndex = 9;
            // 
            // activatorPosYBox
            // 
            this.activatorPosYBox.Location = new System.Drawing.Point(392, 31);
            this.activatorPosYBox.Name = "activatorPosYBox";
            this.activatorPosYBox.Size = new System.Drawing.Size(50, 20);
            this.activatorPosYBox.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "x";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Размер:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(197, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Позиция активатора:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(374, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "x";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frontControl1
            // 
            this.frontControl1.Location = new System.Drawing.Point(12, 65);
            this.frontControl1.Name = "frontControl1";
            this.frontControl1.Size = new System.Drawing.Size(958, 34);
            this.frontControl1.TabIndex = 15;
            // 
            // NeuralWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 648);
            this.Controls.Add(this.frontControl1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.activatorPosYBox);
            this.Controls.Add(this.activatorPosXBox);
            this.Controls.Add(this.sizeYBox);
            this.Controls.Add(this.sizeXBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.generateButton);
            this.Name = "NeuralWindow";
            this.Text = "NeuralWindow";
            this.SizeChanged += new System.EventHandler(this.NeuralWindow_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TextBox sizeXBox;
        private System.Windows.Forms.TextBox sizeYBox;
        private System.Windows.Forms.TextBox activatorPosXBox;
        private System.Windows.Forms.TextBox activatorPosYBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer1;
        private FrontControl frontControl1;
    }
}