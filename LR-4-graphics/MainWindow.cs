﻿using LR_4_graphics.CalculationAPI;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class MainWindow : Form
    {
        DistributionSwitchControll distributionControl;
        private DotNetMatrix currentState;
        private Size size;
        private LifeSimulator simulator;
        private bool isModelling = false;

        private Point[,] horizontalLinePoints;
        private Point[,] verticalLinePoints;
        Pen BlackPen = new Pen(Brushes.Black);

        int width;
        int height;
        double k;

        public MainWindow()
        {
            InitializeComponent();

            distributionControl = new DistributionSwitchControll();
            distributionControl.Location = new Point(10, 10);
            this.Controls.Add(distributionControl);

            timer1.Interval = 100;
        }

        private void generateDimentios()
        {
            k = (double)pictureBox1.Height / size.Height;
            if ((int)(size.Width * k) > pictureBox1.Width)
                k = (double)pictureBox1.Width / size.Width;

            width = (int)k;
            height = (int)k;
        }

        private void generateLinePoints()
        {
            verticalLinePoints = new Point[size.Width + 1, 2];
            horizontalLinePoints = new Point[size.Height + 1, 2];

            for (int i = 0; i <= size.Width; ++i)
            {
                verticalLinePoints[i, 0] = new Point(i * width, 0);
                verticalLinePoints[i, 1] = new Point(i * width, size.Width * width);
            }

            for (int j = 0; j <= size.Height; ++j)
            {
                horizontalLinePoints[j, 0] = new Point(0, j * height);
                horizontalLinePoints[j, 1] = new Point(size.Width * width, j * height);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
            generateDimentios();
            generateLinePoints();

            Draw();
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            try
            {
                size = distributionControl.size;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Неверно задан размер");
                return;
            }

            if (distributionControl.isNormal())
            {
                NormalDistribution n_dist = new NormalDistribution(
                    size.Width / 2, size.Height / 2,
                    distributionControl.sigma_1, distributionControl.sigma_2
                    );
                Configuration config = new Configuration(true, size, n_dist);
                simulator = new LifeSimulator(config);
            }
            else
            {
                EvenDistribution e_dist = new EvenDistribution(
                                     distributionControl.count);
                Configuration config = new Configuration(true, size, e_dist);
                simulator = new LifeSimulator(config);
            }

            generateDimentios();
            generateLinePoints();

            currentState = simulator.getState();
            Draw();
        }

        private void Draw()
        {
            var bmp = new Bitmap((int)(k * size.Width) + 1, (int)(k * size.Height) + 1);

            var image = Graphics.FromImage(bmp);
            image.Clear(SystemColors.Control);

            var matrix = currentState;

            for (int i = 0; i <= size.Width; ++i)
            {
                image.DrawLine(BlackPen, verticalLinePoints[i, 0], verticalLinePoints[i, 1]);
            }

            for (int j = 0; j <= size.Height; ++j)
            {
                image.DrawLine(BlackPen, horizontalLinePoints[j, 0], horizontalLinePoints[j, 1]);
            }

            for (int i = 0; i < size.Width; ++i)
            {
                for (int j = 0; j < size.Height; ++j)
                {
                    if (currentState[j, i])
                    {
                        Rectangle rect = new Rectangle(i * width, j * height, width, height);
                        image.FillRectangle(Brushes.Red, rect);
                    }
                }
            }

            pictureBox1.Image = bmp;
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            //if (!isModelling)
            //{
            //    isModelling = true;
            //    timer1.Start();
            //    playButton.Text = "Stop";
            //}
            //else
            //{
            //    isModelling = false;
            //    timer1.Stop();
            //    playButton.Text = "Play";
            //}
            currentState = simulator.nextState();
            Draw();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            currentState = simulator.nextState();
            Draw();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            UInt32 i = Convert.ToUInt32(e.X / width);
            UInt32 j = Convert.ToUInt32(e.Y / height);
            simulator.setAutomate(j, i);

            currentState = simulator.getState();
            Draw();
        }
    }
}
