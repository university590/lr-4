﻿namespace LR_4_graphics
{
    partial class FrontControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.k1Box = new System.Windows.Forms.TextBox();
            this.b1box = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.b2Box = new System.Windows.Forms.TextBox();
            this.k2Box = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.b3Box = new System.Windows.Forms.TextBox();
            this.k3Box = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.b4Box = new System.Windows.Forms.TextBox();
            this.k4Box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Кол-во фронтов:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.comboBox1.Location = new System.Drawing.Point(101, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // k1Box
            // 
            this.k1Box.Location = new System.Drawing.Point(273, 5);
            this.k1Box.Name = "k1Box";
            this.k1Box.Size = new System.Drawing.Size(43, 20);
            this.k1Box.TabIndex = 2;
            // 
            // b1box
            // 
            this.b1box.Location = new System.Drawing.Point(350, 6);
            this.b1box.Name = "b1box";
            this.b1box.Size = new System.Drawing.Size(43, 20);
            this.b1box.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "k:=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "b:=";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(508, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "b:=";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(431, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "k:=";
            // 
            // b2Box
            // 
            this.b2Box.Location = new System.Drawing.Point(536, 6);
            this.b2Box.Name = "b2Box";
            this.b2Box.Size = new System.Drawing.Size(43, 20);
            this.b2Box.TabIndex = 14;
            // 
            // k2Box
            // 
            this.k2Box.Location = new System.Drawing.Point(459, 5);
            this.k2Box.Name = "k2Box";
            this.k2Box.Size = new System.Drawing.Size(43, 20);
            this.k2Box.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(692, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "b:=";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(615, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "k:=";
            // 
            // b3Box
            // 
            this.b3Box.Location = new System.Drawing.Point(720, 6);
            this.b3Box.Name = "b3Box";
            this.b3Box.Size = new System.Drawing.Size(43, 20);
            this.b3Box.TabIndex = 18;
            // 
            // k3Box
            // 
            this.k3Box.Location = new System.Drawing.Point(643, 5);
            this.k3Box.Name = "k3Box";
            this.k3Box.Size = new System.Drawing.Size(43, 20);
            this.k3Box.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(878, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "b:=";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(801, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "k:=";
            // 
            // b4Box
            // 
            this.b4Box.Location = new System.Drawing.Point(906, 6);
            this.b4Box.Name = "b4Box";
            this.b4Box.Size = new System.Drawing.Size(43, 20);
            this.b4Box.TabIndex = 22;
            // 
            // k4Box
            // 
            this.k4Box.Location = new System.Drawing.Point(829, 5);
            this.k4Box.Name = "k4Box";
            this.k4Box.Size = new System.Drawing.Size(43, 20);
            this.k4Box.TabIndex = 21;
            // 
            // FrontControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.b4Box);
            this.Controls.Add(this.k4Box);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.b3Box);
            this.Controls.Add(this.k3Box);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.b2Box);
            this.Controls.Add(this.k2Box);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.b1box);
            this.Controls.Add(this.k1Box);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "FrontControl";
            this.Size = new System.Drawing.Size(958, 34);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox k1Box;
        private System.Windows.Forms.TextBox b1box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox b2Box;
        private System.Windows.Forms.TextBox k2Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox b3Box;
        private System.Windows.Forms.TextBox k3Box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox b4Box;
        private System.Windows.Forms.TextBox k4Box;
    }
}
