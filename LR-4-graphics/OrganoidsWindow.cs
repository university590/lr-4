﻿using LR_4_graphics.CalculationAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class OrganoidsWindow : Form
    {
        DistributionSwitchControll distributionControl;
        private DotNetMatrix currentState;
        private Size size;
        private OrganoidSimulator simulator;
        private bool isModelling = false;

        private Point [,] horizontalLinePoints;
        private Point[,] verticalLinePoints;
        Pen BlackPen = new Pen(Brushes.Black);

        int width;
        int height;
        double k;

        public OrganoidsWindow()
        {
            InitializeComponent();
            distributionControl = new DistributionSwitchControll();
            distributionControl.Location = new Point(10, 10);
            this.Controls.Add(distributionControl);

            comboBox1.SelectedIndex = 0;

            timer1.Interval = 100;
        }

        private void generateDimentios()
        {
            k = (double)pictureBox1.Height / size.Height;
            if ((int)(size.Width * k) > pictureBox1.Width)
                k = (double)pictureBox1.Width / size.Width;

            width = (int)k;
            height = (int)k;
        }

        private void generateLinePoints()
        {
            verticalLinePoints = new Point[size.Width + 1, 2];
            horizontalLinePoints = new Point[size.Height + 1, 2];

            for (int i = 0; i <= size.Width; ++i)
            {
                verticalLinePoints[i, 0] = new Point(i * width, 0);
                verticalLinePoints[i, 1] = new Point(i * width, size.Width * width);
            }

            for (int j = 0; j <= size.Height; ++j)
            {
                horizontalLinePoints[j, 0] = new Point(0, j * height);
                horizontalLinePoints[j, 1] = new Point(size.Width * width, j * height);
            }
        }

        private void OrganoidsWindow_Load(object sender, EventArgs e)
        {

        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            try
            {
                size = distributionControl.size;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Неверно задан размер");
                return;
            }

            if (distributionControl.isNormal())
            {
                NormalDistribution n_dist = new NormalDistribution(
                    size.Width / 2, size.Height / 2,
                    distributionControl.sigma_1, distributionControl.sigma_2
                    );
                Configuration config = new Configuration(true, size, n_dist);
                config.SmartOrganoids = Convert.ToBoolean(comboBox1.SelectedIndex);
                simulator = new OrganoidSimulator(config);
            }
            else
            {
                EvenDistribution e_dist = new EvenDistribution(
                                     distributionControl.count);
                Configuration config = new Configuration(true, size, e_dist);
                config.SmartOrganoids = Convert.ToBoolean(comboBox1.SelectedIndex);
                simulator = new OrganoidSimulator(config);
            }

            generateDimentios();
            generateLinePoints();

            currentState = simulator.getState();
            Draw();
        }

        private void Draw()
        {
            var bmp = new Bitmap((int)(k * size.Width) + 1, (int)(k * size.Height) + 1);

            var image = Graphics.FromImage(bmp);
            image.Clear(SystemColors.Control);

            var matrix = currentState;

            for (int i = 0; i <= size.Width; ++i)
            {
                image.DrawLine(BlackPen, verticalLinePoints[i, 0], verticalLinePoints[i, 1]);
            }

            for (int j = 0; j <= size.Height; ++j)
            {
                image.DrawLine(BlackPen, horizontalLinePoints[j, 0], horizontalLinePoints[j, 1]);
            }

            for (int i = 0; i < size.Width; ++i)
            {
                for (int j = 0; j < size.Height; ++j)
                {
                    if (currentState[j, i])
                    {
                        Rectangle rect = new Rectangle(i * width, j * height, width, height);
                        image.FillRectangle(Brushes.Red, rect);
                    }
                }
            }

            pictureBox1.Image = bmp;
        }

        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
            generateDimentios();
            generateLinePoints();

            Draw();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            currentState = simulator.nextState();
            Draw();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            if (!isModelling)
            {
                isModelling = true;
                timer1.Start();
                playButton.Text = "Stop";
            }
            else
            {
                isModelling = false;
                timer1.Stop();
                playButton.Text = "Play";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
