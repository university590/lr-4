﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LR_4_graphics.CalculationAPI;

namespace LR_4_graphics
{
    public partial class FrontControl : UserControl
    {
        List<Label> label_k, label_b;
        List<TextBox> k, b;

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public FrontControl()
        {
            InitializeComponent();
            label_k = new List<Label>();
            label_b = new List<Label>();
            k = new List<TextBox>();
            b = new List<TextBox>();

            label_k.Add(label2);
            label_k.Add(label4);
            label_k.Add(label6);
            label_k.Add(label8);

            label_b.Add(label3);
            label_b.Add(label5);
            label_b.Add(label7);
            label_b.Add(label9);

            k.Add(k1Box);
            k.Add(k2Box);
            k.Add(k3Box);
            k.Add(k4Box);

            b.Add(b1box);
            b.Add(b2Box);
            b.Add(b3Box);
            b.Add(b4Box);

            comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < comboBox1.SelectedIndex; ++i)
            {
                label_k[i].Visible = true;
                label_b[i].Visible = true;
                k[i].Visible = true;
                b[i].Visible = true;
            }
            for (int i = comboBox1.SelectedIndex; i < 4; ++i)
            {
                label_k[i].Visible = false;
                label_b[i].Visible = false;
                k[i].Visible = false;
                b[i].Visible = false;
            }
        }

        public List<Front> getFrontsList()
        {
            List<Front> res = new List<Front>();
            for (int i = 0; i < comboBox1.SelectedIndex; ++i)
            {
                res.Add(new Front(Convert.ToDouble(k[i].Text), Convert.ToDouble(b[i].Text)));
            }
            return res;
        }
            
    }
}
