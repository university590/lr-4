﻿namespace LR_4_graphics
{
    partial class DistributionSwitchControll
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.distributionBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sigma1Label = new System.Windows.Forms.Label();
            this.countLabel = new System.Windows.Forms.Label();
            this.sigma1CountBox = new System.Windows.Forms.TextBox();
            this.sigma2Label = new System.Windows.Forms.Label();
            this.sigma2Box = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // distributionBox
            // 
            this.distributionBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Нормальное",
            "Равномерное"});
            this.distributionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.distributionBox.FormattingEnabled = true;
            this.distributionBox.Items.AddRange(new object[] {
            "Нормальное",
            "Равномерное"});
            this.distributionBox.Location = new System.Drawing.Point(125, 7);
            this.distributionBox.Name = "distributionBox";
            this.distributionBox.Size = new System.Drawing.Size(121, 24);
            this.distributionBox.TabIndex = 0;
            this.distributionBox.SelectedIndexChanged += new System.EventHandler(this.distributionBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Распределение:";
            // 
            // sigma1Label
            // 
            this.sigma1Label.AutoSize = true;
            this.sigma1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sigma1Label.Location = new System.Drawing.Point(272, 12);
            this.sigma1Label.Name = "sigma1Label";
            this.sigma1Label.Size = new System.Drawing.Size(121, 16);
            this.sigma1Label.TabIndex = 2;
            this.sigma1Label.Text = "Отклонение по X:";
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countLabel.Location = new System.Drawing.Point(256, 12);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(137, 16);
            this.countLabel.TabIndex = 3;
            this.countLabel.Text = "Количество клеток:";
            this.countLabel.Click += new System.EventHandler(this.countLabel_Click);
            // 
            // sigma1CountBox
            // 
            this.sigma1CountBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sigma1CountBox.Location = new System.Drawing.Point(399, 7);
            this.sigma1CountBox.Name = "sigma1CountBox";
            this.sigma1CountBox.Size = new System.Drawing.Size(53, 22);
            this.sigma1CountBox.TabIndex = 4;
            // 
            // sigma2Label
            // 
            this.sigma2Label.AutoSize = true;
            this.sigma2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sigma2Label.Location = new System.Drawing.Point(458, 10);
            this.sigma2Label.Name = "sigma2Label";
            this.sigma2Label.Size = new System.Drawing.Size(122, 16);
            this.sigma2Label.TabIndex = 5;
            this.sigma2Label.Text = "Отклонение по Y:";
            // 
            // sigma2Box
            // 
            this.sigma2Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sigma2Box.Location = new System.Drawing.Point(587, 7);
            this.sigma2Box.Name = "sigma2Box";
            this.sigma2Box.Size = new System.Drawing.Size(53, 22);
            this.sigma2Box.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(646, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Размер:";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(701, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(53, 22);
            this.textBox1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(760, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "x";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(778, 7);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(53, 22);
            this.textBox2.TabIndex = 10;
            // 
            // DistributionSwitchControll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sigma2Box);
            this.Controls.Add(this.sigma2Label);
            this.Controls.Add(this.sigma1CountBox);
            this.Controls.Add(this.countLabel);
            this.Controls.Add(this.sigma1Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.distributionBox);
            this.Name = "DistributionSwitchControll";
            this.Size = new System.Drawing.Size(854, 43);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox distributionBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label sigma1Label;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.TextBox sigma1CountBox;
        private System.Windows.Forms.Label sigma2Label;
        private System.Windows.Forms.TextBox sigma2Box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
    }
}
