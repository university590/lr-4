﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class DistributionSwitchControl : UserControl
    {
        private int defaultCount = 500;
        private int defaultSigma_1 = 50;
        private int defaultSigma_2 = 50;
        private int count = 500;
        private int sigma_1 = 50;
        private int sigma_2 = 50;

        public DistributionSwitchControl()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void DistributionSwitchControl_Load(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBox1.SelectedIndex)
            {
                case 0:
                    cellCountLable.Visible = false;
                    sigmaXLable.Visible = true;
                    sigmaYLable.Visible = true;
                    sigmaYBox.Visible = true;
                    sigmaYBox.Text = defaultSigma_1.ToString();
                    countOrSigmaXBox.Text = defaultSigma_1.ToString();
                    break;
                case 1:
                    cellCountLable.Visible = true;
                    sigmaXLable.Visible = false;
                    sigmaYLable.Visible = false;
                    sigmaYBox.Visible = false;
                    countOrSigmaXBox.Text = defaultCount.ToString();
                    break;
            }
        }

        private void countOrSigmaXBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DistributionSwitchControl
            // 
            this.Name = "DistributionSwitchControl";
            this.Size = new System.Drawing.Size(667, 168);
            this.ResumeLayout(false);

        }
    }
}
