﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class ChooseForm : Form
    {
        public ChooseForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainWindow lifeWindow = new MainWindow();
            this.Hide();
            lifeWindow.ShowDialog();
            this.Show();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ChooseForm_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            NeuralWindow neuralWindow = new NeuralWindow();
            this.Hide();
            neuralWindow.ShowDialog();
            this.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OrganoidsWindow neuralWindow = new OrganoidsWindow();
            this.Hide();
            neuralWindow.ShowDialog();
            this.Show();
        }
    }
}
