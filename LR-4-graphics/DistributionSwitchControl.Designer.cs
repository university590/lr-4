﻿namespace LR_4_graphics
{
    partial class DistributionSwitchControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.countOrSigmaXBox = new System.Windows.Forms.TextBox();
            this.sigmaYBox = new System.Windows.Forms.TextBox();
            this.sigmaXLable = new System.Windows.Forms.Label();
            this.sigmaYLable = new System.Windows.Forms.Label();
            this.cellCountLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Распределение";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Нормальное",
            "Равномерное"});
            this.comboBox1.Location = new System.Drawing.Point(96, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // countOrSigmaXBox
            // 
            this.countOrSigmaXBox.Location = new System.Drawing.Point(354, 0);
            this.countOrSigmaXBox.Name = "countOrSigmaXBox";
            this.countOrSigmaXBox.Size = new System.Drawing.Size(70, 20);
            this.countOrSigmaXBox.TabIndex = 2;
            this.countOrSigmaXBox.TextChanged += new System.EventHandler(this.countOrSigmaXBox_TextChanged);
            // 
            // sigmaYBox
            // 
            this.sigmaYBox.Location = new System.Drawing.Point(561, 0);
            this.sigmaYBox.Name = "sigmaYBox";
            this.sigmaYBox.Size = new System.Drawing.Size(70, 20);
            this.sigmaYBox.TabIndex = 3;
            // 
            // sigmaXLable
            // 
            this.sigmaXLable.AutoSize = true;
            this.sigmaXLable.Location = new System.Drawing.Point(223, 3);
            this.sigmaXLable.Name = "sigmaXLable";
            this.sigmaXLable.Size = new System.Drawing.Size(125, 13);
            this.sigmaXLable.TabIndex = 4;
            this.sigmaXLable.Text = "Ср.кв.отклонение по X:";
            // 
            // sigmaYLable
            // 
            this.sigmaYLable.AutoSize = true;
            this.sigmaYLable.Location = new System.Drawing.Point(430, 3);
            this.sigmaYLable.Name = "sigmaYLable";
            this.sigmaYLable.Size = new System.Drawing.Size(125, 13);
            this.sigmaYLable.TabIndex = 5;
            this.sigmaYLable.Text = "Ср.кв.отклонение по Y:";
            // 
            // cellCountLable
            // 
            this.cellCountLable.AutoSize = true;
            this.cellCountLable.Location = new System.Drawing.Point(223, 3);
            this.cellCountLable.Name = "cellCountLable";
            this.cellCountLable.Size = new System.Drawing.Size(107, 13);
            this.cellCountLable.TabIndex = 6;
            this.cellCountLable.Text = "Количество клеток:";
            // 
            // DistributionSwitchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cellCountLable);
            this.Controls.Add(this.sigmaYLable);
            this.Controls.Add(this.sigmaXLable);
            this.Controls.Add(this.sigmaYBox);
            this.Controls.Add(this.countOrSigmaXBox);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "DistributionSwitchControl";
            this.Size = new System.Drawing.Size(642, 130);
            this.Load += new System.EventHandler(this.DistributionSwitchControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox countOrSigmaXBox;
        private System.Windows.Forms.TextBox sigmaYBox;
        private System.Windows.Forms.Label sigmaXLable;
        private System.Windows.Forms.Label sigmaYLable;
        private System.Windows.Forms.Label cellCountLable;
    }
}
