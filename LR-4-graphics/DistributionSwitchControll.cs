﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace LR_4_graphics
{
    public partial class DistributionSwitchControll : UserControl
    {
        private int defaultCount = 500;
        private int defaultSigma_1 = 50;
        private int defaultSigma_2 = 50;

        public DistributionSwitchControll()
        {
            InitializeComponent();
            distributionBox.SelectedIndex = 0;
        }

        private void distributionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (distributionBox.SelectedIndex)
            {
                case 0:
                    countLabel.Visible = false;
                    sigma1Label.Visible = true;
                    sigma2Label.Visible = true;
                    sigma2Box.Visible = true;
                    sigma1CountBox.Text = defaultSigma_1.ToString();
                    sigma2Box.Text = defaultSigma_2.ToString();
                    break;
                case 1:
                    countLabel.Visible = true;
                    sigma1Label.Visible = false;
                    sigma2Label.Visible = false;
                    sigma2Box.Visible = false;
                    sigma1CountBox.Text = defaultCount.ToString();
                    break;
            }
        }

        private void countLabel_Click(object sender, EventArgs e)
        {

        }

        public bool isNormal()
        {
            return distributionBox.SelectedIndex == 0;
        }

        public bool isEven()
        {
            return distributionBox.SelectedIndex == 1;
        }

        public Size size
        {
            get
            {
                Size res = new Size();
                res.Width = Convert.ToInt32(textBox1.Text);
                res.Height = Convert.ToInt32(textBox2.Text);
                return res;
            }
            private set { }
        }

        public int count
        {
            get
            {
                return Convert.ToInt32(sigma1CountBox.Text);
            }
            private set { }
        }

        public int sigma_1
        {
            get
            {
                return Convert.ToInt32(sigma1CountBox.Text);
            }
            private set { }
        }
        public int sigma_2
        {
            get
            {
                return Convert.ToInt32(sigma2Box.Text);
            }
            private set { }
        }
    }
}
