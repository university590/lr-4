﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace LR_4_graphics.CalculationAPI
{
    class DotNetMatrix : IDisposable
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int dotNetMatrix_get(IntPtr obj, int i, int j);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void delete_matrix(IntPtr obj);

        private IntPtr m_obj;

        public DotNetMatrix(IntPtr obj)
        {
            m_obj = obj;
        }

        public bool this [int i, int j]
        {
            get
            {
                return Convert.ToBoolean(dotNetMatrix_get(m_obj, i, j));
            }
            private set { }
        }

        public void Dispose()
        {
            delete_matrix(m_obj);
        }

    }
}
