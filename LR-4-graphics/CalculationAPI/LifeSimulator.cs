﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace LR_4_graphics.CalculationAPI
{
    class LifeSimulator
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_life_simulator(IntPtr configuration);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr get_next_state(IntPtr obj);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr get_state(IntPtr obj);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void setLifeAutomate(IntPtr obj, UInt32 i, UInt32 j);

        private IntPtr m_obj;
        Configuration configuration;

        public LifeSimulator(Configuration configuration)
        {
            this.configuration = configuration;
            m_obj = create_life_simulator(configuration.impl());
        }

        public DotNetMatrix nextState()
        {
            return new DotNetMatrix(get_next_state(m_obj));
        }

        public DotNetMatrix getState()
        {
            return new DotNetMatrix(get_state(m_obj));
        }

        public void setAutomate(UInt32 i, UInt32 j)
        {
            setLifeAutomate(m_obj, i, j);
        }
    }
}
