﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LR_4_graphics.CalculationAPI
{
    class OrganoidSimulator
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr organoid_simulator_network(IntPtr configuration);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr o_get_state(IntPtr obj);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr o_next_state(IntPtr obj);

        private IntPtr m_obj;
        Configuration configuration;

        public OrganoidSimulator(Configuration configuration)
        {
            this.configuration = configuration;
            m_obj = organoid_simulator_network(configuration.impl());
        }

        public DotNetMatrix nextState()
        {
            return new DotNetMatrix(o_next_state(m_obj));
        }

        public DotNetMatrix getState()
        {
            return new DotNetMatrix(o_get_state(m_obj));
        }
    }
}
