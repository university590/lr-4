﻿using System;
using System.Runtime.InteropServices;

namespace LR_4_graphics.CalculationAPI
{
    class NormalDistribution
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_normal_distribution(double m_1,
                                double m_2, double sigma_1, double sigma_2);

        private IntPtr m_obj;

        public NormalDistribution(double m_1, double m_2, double sigma_1, double sigma_2)
        {
            m_obj = create_normal_distribution(m_1, m_2, sigma_1, sigma_2);
        }

        public IntPtr impl()
        {
            return m_obj;
        }
    }
}
