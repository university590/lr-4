﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;

namespace LR_4_graphics.CalculationAPI
{
    public class Front
    {
        public double k { get; set; }
        public double b { get; set; }

        public Front(double k, double b)
        {
            this.k = k;
            this.b = b;
        }
    }

    class Configuration
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_configuration_normal_dist(bool ompSupport, int x,
                                                        int y, IntPtr distribution);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_configuration_even_dist(bool ompSupport, int x,
                                                int y, IntPtr distribution);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_configuration(bool ompSupport, int x, int y);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr add_front(IntPtr obj, double k, double b);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr isSmartOrganoidsEnabled(IntPtr obj, bool enabled);

        private IntPtr m_obj;
        public Size size { get; set; }
        NormalDistribution normal_distribution;
        EvenDistribution even_distribution;
        List<Front> m_fronts;
        bool smartOrganoids;

        public Configuration(bool ompSupport, Size size, NormalDistribution dist)
        {
            this.size = size;
            normal_distribution = dist;
            m_obj = create_configuration_normal_dist(ompSupport, size.Width, size.Height, dist.impl());
        }

        public Configuration(bool ompSupport, Size size, EvenDistribution dist)
        {
            this.size = size;
            even_distribution = dist;
            m_obj = create_configuration_even_dist(ompSupport, size.Width, size.Height, dist.impl());
        }

        public Configuration(bool ompSupport, Size size)
        {
            this.size = size;
            m_obj = create_configuration(ompSupport, size.Width, size.Height);
        }

        public List<Front> fronts
        {
            get
            {
                return m_fronts;
            }
            set
            {
                m_fronts = value;

                foreach (var front in m_fronts)
                    add_front(m_obj, front.k, front.b);
            }
        }

        public IntPtr impl()
        {
            return m_obj;
        }

        public bool SmartOrganoids
        {
            get
            {
                return smartOrganoids;
            }

            set
            {
                smartOrganoids = value;
                isSmartOrganoidsEnabled(m_obj, value);
            }
        }
    }
}
