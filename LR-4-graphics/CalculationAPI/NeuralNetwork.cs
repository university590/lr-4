﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LR_4_graphics.CalculationAPI
{
    class NeuralNetwork
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_neural_network(IntPtr configuration, IntPtr act);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr n_get_next_state(IntPtr obj);

        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr n_get_state(IntPtr obj);

        private IntPtr m_obj;
        Configuration configuration;
        NeuralActivator activator;

        public NeuralNetwork(Configuration configuration, int x, int y)
        {
            this.configuration = configuration;
            activator = new NeuralActivator(x, y);
            m_obj = create_neural_network(configuration.impl(), activator.impl());
        }

        public DotNetMatrix nextState()
        {
            return new DotNetMatrix(n_get_next_state(m_obj));
        }

        public DotNetMatrix getState()
        {
            return new DotNetMatrix(n_get_state(m_obj));
        }

    }
}
