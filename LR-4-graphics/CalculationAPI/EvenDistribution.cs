﻿using System;
using System.Runtime.InteropServices;

namespace LR_4_graphics.CalculationAPI
{
    class EvenDistribution
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_even_distribution(int count);

        private IntPtr m_obj;

        public EvenDistribution(int count)
        {
            m_obj = create_even_distribution(count);
        }

        public IntPtr impl()
        {
            return m_obj;
        }
    }
}
