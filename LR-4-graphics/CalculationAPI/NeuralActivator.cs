﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LR_4_graphics.CalculationAPI
{
    class NeuralActivator
    {
        [DllImport("LR-4-calculation.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr create_neural_activator(int x, int y);

        private IntPtr m_obj;

        public NeuralActivator(int x, int y)
        {
            m_obj = create_neural_activator(x, y);
        }

        public IntPtr impl()
        {
            return m_obj;
        }
    }
}
